//
//  Weather.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 25/12/2017.
//  Copyright © 2017 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

struct Prevision: Codable {
    let city: City
    let forecastDays: [Forecast]
    
    private enum CodingKeys: String, CodingKey {
        case forecastDays = "list"
        case city
    }
}

struct Forecast: Codable {
    let temperature: Temperature
    let weather: [Weather]
    let date: Int
    
    private enum CodingKeys: String, CodingKey {
        case temperature = "temp"
        case date = "dt"
        case weather
    }
}

struct City: Codable {
    let country: String
    let name: String
}

struct Temperature: Codable {
    let day: Float
    let min: Float
    let max: Float
}

struct Weather: Codable {
    let main: String
    let description: String
    let icon: String
}


