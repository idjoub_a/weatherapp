//
//  WeatherNetworkManager.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 20/05/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

struct WeatherNetworkManager: NetworkManager {
    
    let router = Router<WeatherApi>()
    static var apiKey = "edef4985c1e5777dee5ccc8f3ee062e0"
    
    static var lat: Double = 0.0
    static var long: Double = 0.0
    
    mutating func setCoordinate(_ latitude: Double, _ longitude: Double) {
        WeatherNetworkManager.lat = latitude
        WeatherNetworkManager.long = longitude
    }
    
    func getModel<T : Decodable>(ofType: T.Type, completion: @escaping (Result<T, NetworkErrorResponse>) -> ()) {
        
        router.request(.weatherDaily) { result in
            switch result {
            case .failure(_):
                completion(.failure(NetworkErrorResponse.failed))
            case .success(let data):
                do {
                    let _ = try JSONSerialization.jsonObject(with: data, options: .mutableContainers)
                    let apiResponse = try JSONDecoder().decode(T.self, from: data)
                    completion(.success(apiResponse))
                } catch {
                    print(error)
                    completion(.failure(NetworkErrorResponse.badRequest))
                }
            }
        }
    }
}


/*
 fileprivate func handleNetworkResponse(_ response: HTTPURLResponse) -> Result<String> {
 switch response.statusCode {
 case 200...299: return .success
 case 401...500: return .failure(NetworkResponse.authenticationError.rawValue)
 case 501...599: return .failure(NetworkResponse.badRequest.rawValue)
 case 600: return .failure(NetworkResponse.outdated.rawValue)
 default: return .failure(NetworkResponse.failed.rawValue)
 }
 }*/

