//
//  NetworkManager.swift
//  NetworkLayer
//
//  Created by Malcolm Kumwenda on 2018/03/11.
//  Copyright © 2018 Malcolm Kumwenda. All rights reserved.
//

import Foundation

protocol NetworkManager {
    associatedtype MyRouter
    var router: MyRouter { get }
    static var apiKey: String { get }
    func getModel<T: Decodable>(ofType: T.Type, completion: @escaping (Result<T, NetworkErrorResponse>) -> ())
}

enum NetworkErrorResponse: Error {
    case authenticationError
    case badRequest
    case outdated
    case failed
    case noData
    case unableToDecode
    
    var description: String {
        switch self {
        case .authenticationError:
            return "You need to be authenticated first."
        case .badRequest:
            return "Bad request"
        case .outdated:
            return "The url you requested is outdated."
        case .failed:
            return "Network request failed."
        case .noData:
            return "Response returned with no data to decode."
        case .unableToDecode:
            return "We could not decode the response."
        }
    }
}
