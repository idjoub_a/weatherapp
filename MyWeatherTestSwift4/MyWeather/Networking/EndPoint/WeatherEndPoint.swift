//
//  MovieEndPoint.swift
//  NetworkLayer
//
//  Created by Malcolm Kumwenda on 2018/03/07.
//  Copyright © 2018 Malcolm Kumwenda. All rights reserved.
//

import Foundation

public enum WeatherApi {
    case weatherDaily
}

extension WeatherApi: EndPointType {
    
 //"http://api.openweathermap.org/data/2.5/forecast/daily?units=metric&cnt=7&lang=en&APPID=edef4985c1e5777dee5ccc8f3ee062e0&lat=\(strLatitude)&lon=\(strLongitude)"
    
    var environmentBaseURL: String {
        switch self {
        case .weatherDaily: return "http://api.openweathermap.org/data/2.5/forecast"
        }
    }
    
    var baseURL: URL {
        guard let url = URL(string: environmentBaseURL) else { fatalError("baseURL could not be configured.")}
        return url
    }
    
    var path: String {
        switch self {
        case .weatherDaily:
            return "/daily"
        }
    }
    
    var httpMethod: HTTPMethod {
        return .get
    }
    
    var task: HTTPTask {
        switch self {
        case .weatherDaily:
            return .requestParameters(bodyParameters: nil,
                                      bodyEncoding: .urlEncoding,
                                      urlParameters: ["units":"metric",
                                                      "cnt":"7",
                                                      "lang":"en",
                                                      "APPID":WeatherNetworkManager.apiKey,
                                                      "lat":WeatherNetworkManager.lat,
                                                      "lon":WeatherNetworkManager.long])
        }
    }
    
    var headers: HTTPHeaders? {
        return nil
    }
}


