//
//  GlobalFunctions.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 03/01/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import Foundation

func getDate(unixdate: Int, timezone: String = "AST") -> String {
    if unixdate == 0 { return "" }
    let date = NSDate(timeIntervalSince1970: TimeInterval(unixdate))
    let dayTimePeriodFormatter = DateFormatter()
    dayTimePeriodFormatter.dateFormat = "EEE"
    dayTimePeriodFormatter.timeZone = NSTimeZone(name: timezone) as TimeZone?
    let dateString = dayTimePeriodFormatter.string(from: date as Date)
    return dateString
}
