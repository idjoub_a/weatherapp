//
//  LoadingViewController.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 13/05/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import CoreLocation

protocol SendingDataDelegate: class {
    func userDidGetData(_ data: Codable)
}

class LoadingViewController: UIViewController {

    @IBOutlet weak var loadingIndicator: UIActivityIndicatorView!
    
    weak var delegate: SendingDataDelegate?
    
    var networkManager: WeatherNetworkManager?
    
    override func viewDidLoad() {
        super.viewDidLoad()

        loadingIndicator.startAnimating()
    }
    
    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        self.fetchData()
    }
    
    private func fetchData() {

        networkManager?.getModel(ofType: Prevision.self) { [unowned self] result in
            switch result {
            case .failure(let error):
                let alert = UIAlertController(title: "Error", message: "Request had error: \(error.description)", preferredStyle: .alert)
                alert.addAction(UIAlertAction(title: "Retry", style: .default, handler: { _ in
                    self.fetchData()
                }))
                alert.addAction(UIAlertAction(title: "Close App", style: .destructive, handler: { _ in
                    fatalError()
                }))
                DispatchQueue.main.async {
                    self.present(alert, animated: true, completion: nil)
                }
            case .success(let prevision):
                self.delegate?.userDidGetData(prevision)
                DispatchQueue.main.async {
                    self.loadingIndicator.stopAnimating()
                    self.dismiss(animated: true, completion: nil)
                }
            }
        }
    }
    
    @IBAction func cancelRequest(_ sender: UIButton) {
        networkManager?.router.cancel()
        self.loadingIndicator.stopAnimating()
        self.dismiss(animated: true, completion: nil)
    }
}
