//
//  MainViewController.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 26/12/2017.
//  Copyright © 2017 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit
import CoreLocation
import SDWebImage
// https://medium.com/flawless-app-stories/writing-network-layer-in-swift-protocol-oriented-approach-4fa40ef1f908

enum TitleMeteoPic: String {
    case sun = "☀️"
    case cloudAndSun = "🌤"
    case cloud = "☁️"
    case rain = "🌧"
    case rainAndSun = "🌦"
    case lightning = "⚡️"
    case snow = "❄️"
    case wind = "💨"
    case none = ""
}

class MainViewController: UIViewController {

    @IBOutlet weak var background: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    @IBOutlet weak var dailyDetails: UICollectionView!

    private var networkManager = WeatherNetworkManager()
    private let locationManager = CLLocationManager()
    private var locationIsUpdated: Bool = false
    private var myCellID = "DayDetailWeatherCollectionViewCell"
    private var prevision: Prevision? = nil {
        didSet { self.printData() }
    }

    override func viewDidLoad() {
        super.viewDidLoad()

        self.navigationController?.navigationBar.setBackgroundImage(UIImage(), for: .default)
        self.navigationController?.navigationBar.shadowImage = UIImage()
        self.navigationController?.navigationBar.isTranslucent = true
        self.navigationController?.navigationBar.tintColor = UIColor.white
        self.navigationController?.navigationBar.titleTextAttributes = [NSAttributedString.Key.foregroundColor : UIColor.white,
                                                                        NSAttributedString.Key.font: UIFont(name: "Noteworthy-Bold", size: 20.0)!]

        let nibCell = UINib(nibName: self.myCellID, bundle: nil)
        dailyDetails.register(nibCell, forCellWithReuseIdentifier: self.myCellID)
        
        guard let noDataViewXIB = Bundle.main.loadNibNamed("NoDataView", owner: self, options: nil)?.first as? NoDataView else { return }
        noDataViewXIB.tag = 42
        self.view.addSubview(noDataViewXIB)
        
        background.applyMotionEffect(magnitude: 20)
    }
    
    override func viewWillAppear(_ animated: Bool) {
        if !locationIsUpdated {
            self.setCLocation()
            self.openLoadingVC()
        }
    }

    private func openLoadingVC() {
        guard let modal = Bundle.main.loadNibNamed("LoadingViewController", owner: self, options: nil)?.first as? LoadingViewController
        else { return }
        modal.modalPresentationStyle = UIModalPresentationStyle.overCurrentContext
        modal.networkManager = self.networkManager
        modal.delegate = self
        self.present(modal, animated: true, completion: nil)
    }
    
    private func getLocation(from city: String, completion: @escaping (Result<CLLocationCoordinate2D?, Error>) -> Void) {
        CLGeocoder().geocodeAddressString(city) { placemarks, error in
            if let error = error {
                completion(.failure(error))
            }
            else if let coordinate = placemarks?.first?.location?.coordinate {
                completion(.success(coordinate))
            }
        }
    }

    @objc private func openSearchCityAlert() {
        let alert = UIAlertController(title: "Enter city name", message: nil, preferredStyle: .alert)

        alert.addTextField(configurationHandler: { (textField) in
            textField.placeholder = "City"
        })

        alert.addAction(UIAlertAction(title: "Ok", style: .default, handler: { _ in
            if let city = alert.textFields?.first?.text {
                self.getLocation(from: city) { result in
                    switch result {

                    case .failure(_):
                        let error = UIAlertController(title: "Error", message: "City not found", preferredStyle: .alert)
                        error.addAction(UIAlertAction(title: "Ok", style: .default, handler: nil))
                        self.present(error, animated: true, completion: nil)

                    case .success(let coordinate):
                        guard let coordinate = coordinate else { return }
                        self.networkManager.setCoordinate(coordinate.latitude, coordinate.longitude)
                        self.openLoadingVC()
                    }
                }
            }
        }))

        alert.addAction(UIAlertAction(title: "Cancel", style: .destructive, handler: nil))
        self.present(alert, animated: true, completion: nil)
    }
    
    private func getMeteoPic(by id: String) -> TitleMeteoPic.RawValue {
        switch id {
        case "01d":
            return TitleMeteoPic.sun.rawValue
        case "02d":
            return TitleMeteoPic.cloudAndSun.rawValue
        case "03d", "04d":
            return TitleMeteoPic.cloud.rawValue
        case "09d":
            return TitleMeteoPic.rain.rawValue
        case "10d":
            return TitleMeteoPic.rainAndSun.rawValue
        case "11d":
            return TitleMeteoPic.lightning.rawValue
        case "13d":
            return TitleMeteoPic.snow.rawValue
        case "50d":
            return TitleMeteoPic.wind.rawValue
        default:
            return TitleMeteoPic.none.rawValue
        }
    }
    
    private func printData() {
        DispatchQueue.main.async {
            guard let prevision = self.prevision, let todayPrevision = prevision.forecastDays.first else { return }
            
            var titleMeteoEmoji = TitleMeteoPic.none.rawValue
            
            if let iconID = todayPrevision.weather.first?.icon {
                titleMeteoEmoji = self.getMeteoPic(by: iconID)
            }
            
            self.title = "\(prevision.city.name), \(prevision.city.country) \(titleMeteoEmoji)"
            self.temperature.text = "\(String(format: "%.f", todayPrevision.temperature.day.rounded()))°C"
            self.dailyDetails.reloadData()
            self.view.viewWithTag(42)?.isHidden = true
            self.navigationItem.rightBarButtonItem = UIBarButtonItem(barButtonSystemItem: .search, target: self, action: #selector(self.openSearchCityAlert))
        }
    }
}

extension MainViewController: UICollectionViewDataSource, UICollectionViewDelegateFlowLayout {

    func numberOfSections(in collectionView: UICollectionView) -> Int {
        return 1
    }
    
    func collectionView(_ collectionView: UICollectionView, numberOfItemsInSection section: Int) -> Int {
        guard let prevision = self.prevision else {
            return 0
        }
        return prevision.forecastDays.count
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        insetForSectionAt section: Int) -> UIEdgeInsets {
        let inset = 0
        return UIEdgeInsets.init(top: CGFloat(inset), left: CGFloat(inset), bottom: CGFloat(inset), right: CGFloat(inset))
    }
    
    func collectionView(_ collectionView: UICollectionView,
                        layout collectionViewLayout: UICollectionViewLayout,
                        sizeForItemAt indexPath: IndexPath) -> CGSize {
        return CGSize.init(width: 80, height: 80)
    }
    
    func collectionView(_ collectionView: UICollectionView, cellForItemAt indexPath: IndexPath) -> UICollectionViewCell {
        guard let cell = collectionView.dequeueReusableCell(withReuseIdentifier: myCellID, for: indexPath) as? DayDetailWeatherCollectionViewCell
        else { return UICollectionViewCell() }
        
        guard let prevision = self.prevision else {
            return UICollectionViewCell()
        }
        
        cell.day.text = getDate(unixdate: prevision.forecastDays[indexPath.row].date)
        cell.icon.sd_setImage(with: URL(string:"http://openweathermap.org/img/w/\(prevision.forecastDays[indexPath.row].weather.first!.icon).png"), completed: nil)
        cell.temperature.text = "\(String(format: "%.f", prevision.forecastDays[indexPath.row].temperature.day))°C"

        return cell
    }
}

extension MainViewController: UICollectionViewDelegate {
    func collectionView(_ collectionView: UICollectionView, didSelectItemAt indexPath: IndexPath) {
        guard let detailVC = Bundle.main.loadNibNamed("DetailViewController", owner: self, options: nil)?.first as? DetailViewController
        else { return }
        self.navigationController?.pushViewController(detailVC, animated: true)
    }
}

extension MainViewController: SendingDataDelegate {
    func userDidGetData(_ data: Codable) {
        self.prevision = data as? Prevision
    }
}

extension MainViewController: CLLocationManagerDelegate {
    
    private func setCLocation() {
        locationManager.requestAlwaysAuthorization()
        
        if CLLocationManager.locationServicesEnabled() {
            locationManager.delegate = self
            locationManager.distanceFilter = 5000.0
            locationManager.desiredAccuracy = kCLLocationAccuracyBest
            locationManager.startUpdatingLocation()
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didUpdateLocations locations: [CLLocation]) {
        if let coordinate = locations.first?.coordinate, !locationIsUpdated {
            networkManager.setCoordinate(coordinate.latitude, coordinate.longitude)
            locationIsUpdated = true
            print("didUpdateLocations get coordinate: ", coordinate)
        }
    }
    
    func locationManager(_ manager: CLLocationManager, didChangeAuthorization status: CLAuthorizationStatus) {
        if (status == CLAuthorizationStatus.denied) {
            showLocationDisabledPopUp()
        }
    }
    
    private func showLocationDisabledPopUp() {
        let alertController = UIAlertController(title: "Background Location Access Disabled",
                                                message: "In order to get your meteo we need your location",
                                                preferredStyle: .alert)
        
        let cancelAction = UIAlertAction(title: "Cancel", style: .cancel, handler: nil)
        alertController.addAction(cancelAction)
        
        let openAction = UIAlertAction(title: "Open Settings", style: .default) { (action) in
            if let url = URL(string: UIApplication.openSettingsURLString) {
                UIApplication.shared.open(url, options: [:], completionHandler: nil)
            }
        }
        alertController.addAction(openAction)
        
        self.present(alertController, animated: true, completion: nil)
    }
}
