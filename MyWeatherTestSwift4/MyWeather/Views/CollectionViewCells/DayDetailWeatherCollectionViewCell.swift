//
//  DayDetailWeatherCollectionViewCell.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 02/01/2018.
//  Copyright © 2018 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class DayDetailWeatherCollectionViewCell: UICollectionViewCell {

    @IBOutlet weak var day: UILabel!
    @IBOutlet weak var icon: UIImageView!
    @IBOutlet weak var temperature: UILabel!
    override func awakeFromNib() {
        super.awakeFromNib()
        // Initialization code
    }

}
