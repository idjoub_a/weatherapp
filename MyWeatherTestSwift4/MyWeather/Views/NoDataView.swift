//
//  NoDataView.swift
//  MyWeather
//
//  Created by Alvaro IDJOUBAR on 20/05/2019.
//  Copyright © 2019 Alvaro IDJOUBAR. All rights reserved.
//

import UIKit

class NoDataView: UIView {

    @IBOutlet weak var errorMessage: UILabel!
    
    
    override init(frame: CGRect) {
        super.init(frame: frame)
    }
    
    override func awakeFromNib() {
        super.awakeFromNib()
        
        let attributesTitle: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 32),
            .foregroundColor: UIColor.white,
        ]
        
        let attributesMessage: [NSAttributedString.Key: Any] = [
            .font: UIFont.boldSystemFont(ofSize: 20),
            .foregroundColor: UIColor.white,
        ]
        
        let finalString = NSMutableAttributedString(string: "Error\n", attributes: attributesTitle)
        finalString.append(NSMutableAttributedString(string: "No data", attributes: attributesMessage))
        
        errorMessage.attributedText = finalString
    }
    
    required init?(coder aDecoder: NSCoder) {
        super.init(coder: aDecoder)
    }
}
